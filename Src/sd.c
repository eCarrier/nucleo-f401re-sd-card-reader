/**
 * @file sd.c
 * @Synopsis Contains the definition for all sd related manipulations
 * @author Etienne Carrier
 * @version
 * @date 2019-12-31
 */

#include "sd.h"


uint8_t HIGH_SPI = 0xFF;



/**
 * @Synopsis Function to modify a command_frame with a c command number
 *
 * @Param cmd The command to change
 * @Param cmd_nmb The command number
 */
void set_command(Command_frame *cmd, Command_number cmd_nmb)
{
    uint32_t args = No_args;
    if (cmd_nmb == Send_if_cond )
        args = Send_if_cond_args ;
    else if (cmd_nmb == App_send_op_cond)
        args = App_send_op_cond_args;
    // Sets the command with a No_args
    set_command_with_args(cmd, cmd_nmb, args);
}

/**
 * @Synopsis Function to modify a command_frame with a command number and
 * arguments
 *
 * @Param cmd The command to change
 * @Param cmd_nmb The command number
 * @Param args The args (32 bits int)
 */
void set_command_with_args(Command_frame *cmd, Command_number cmd_nmb, uint32_t args)
{
    cmd->command_id = cmd_nmb | 1 << 6;
    set_command_crc(cmd, cmd_nmb);
    cmd->command_args.args_ui32 = args;
}

/**
 * @Synopsis  Function to change the CRC with a given command number
 *
 * @Param cmd The command to change
 * @Param cmd_nmb The command number
 */
void set_command_crc(Command_frame *cmd, Command_number cmd_nmb)
{
    if (cmd_nmb ==  Go_idle_state)
    {
        cmd->command_crc = Go_idle_state_crc;
    }
    else if (cmd_nmb == Send_if_cond )
    {
        cmd->command_crc = Send_if_cond_crc;
    }
    else
    {
        cmd->command_crc = Default_crc;
    }
}

/**
 * @Synopsis
 *
 * @Param hspi
 */
void set_sd_card_to_spi(SPI_HandleTypeDef *hspi)
{
    uint8_t high = 0xFF;
    int i = 0;
    //Set CS to high
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
    //Set mosi to high and Toggle for 74 cycle
    //So basically we have to toggle our clock for 74 cycle
    //Every bit of and uint8_t takes one clock cycle, so we'll
    //do 74/8 = 9.ish so we'll do it 10 times
    for (i = 0; i < 10 ; ++i)
        HAL_SPI_Transmit(hspi, &high, 1 , HAL_MAX_DELAY);
}

/**
 * @Synopsis
 *
 * @Param cmd
 * @Param hspi
 */
void send_command(Command_frame *cmd, SPI_HandleTypeDef *hspi)
{
    HAL_SPI_Transmit(hspi, &(cmd->command_id), 1 , HAL_MAX_DELAY);
    //HAL_SPI_Transmit(hspi, cmd->command_args.args_ui8, 4 , HAL_MAX_DELAY);
    /*
     * I do that because when i memcpy or use the union trick to go from
     * uint32 to uint8[4] the bytes are inverted
     *
     * This might be really inefficient.
     */
    for (int i = 3; i >= 0; --i)
        HAL_SPI_Transmit(hspi, &(cmd->command_args.args_ui8[i]), 1 , HAL_MAX_DELAY);
    HAL_SPI_Transmit(hspi, &(cmd->command_crc), 1 , HAL_MAX_DELAY);
}

/**
 * @Synopsis
 *
 * @Param r
 *
 * @Returns
 */
int check_if_valid (R1response *r)
{
    return !( r->fields.erase_Reset | r->fields.illegal_command |
              r->fields.command_crc_error | r->fields.erase_sequence_error |
              r->fields.address_error | r->fields.parameter_error );

}

/**
 * @Synopsis
 *
 * @Param hspi
 * @Param r
 */
void read_r1_response(SPI_HandleTypeDef *hspi, R1response *r)
{
    r->full_response = 0xFF;
    do
    {
        HAL_SPI_TransmitReceive(hspi, &HIGH_SPI, &r->full_response, 1, HAL_MAX_DELAY);
    } while (r->fields.zero_flag != 0);
}

void read_r3_response(SPI_HandleTypeDef *hspi, R3response *r)
{
    read_r1_response (hspi, &(r->r1_resp));
    HAL_SPI_TransmitReceive(hspi, &HIGH_SPI, r->ocr_arr , 4, HAL_MAX_DELAY);
}

void send_read_r1(SPI_HandleTypeDef *hspi, R1response *r, Command_frame *cmd, Command_number cmd_nmb)
{
    set_command(cmd, cmd_nmb);
    send_command(cmd, hspi);
    read_r1_response(hspi, r);
}
void send_read_r3(SPI_HandleTypeDef *hspi, R3response *r, Command_frame *cmd, Command_number cmd_nmb)
{
    set_command(cmd, cmd_nmb);
    send_command(cmd, hspi);
    read_r3_response(hspi, r);
}

/**
 * @Synopsis Function for SD card initialization
 *
 * This function is following the following SDC initialization flow
 *
 * @Param hspi
 *
 * @Returns
 */
Card_version init_sd_card(SPI_HandleTypeDef *hspi)
{
    uint8_t ccs = 0;
    R1response r ;
    R3response r3;
    Command_frame cmd;
    Card_version card_version = UNKNOWN_CARD;
    uint8_t init_card = 0;
    // Send command 00
    send_read_r1(hspi, &r, &cmd, Go_idle_state);
    if (check_if_valid(&r) && r.full_response == 0x01)
    {
        // Send command 08
        send_read_r3(hspi, &r3, &cmd, Send_if_cond);
        init_card = init_sd_v2(r, r3, &cmd, hspi);
        if (init_card != MISMATCH)
        {
            // If there is no error, cherch for CCS bit
            if ( init_card == NO_ERR)
            {
                ccs = (r3.ocr_arr[3] & (1 << 5)) != 0;
                if (!ccs)
                {
                    // IF ccs is not set, force the block size
                    force_block_size_fat(&cmd, hspi);
                }
                return SD_VER2;
            }
            // if it is not SD2, try for SD1
            else if (init_card == NOT_SD2)
            {
                if (init_sd_v1(&r, &cmd, hspi) == NO_ERR)
                {
                    force_block_size_fat(&cmd, hspi);
                    return SD_VER1;
                }
                // if it is not SD1, try for mmc v3
                else
                {
                   if (init_mmc_v3(&r, &cmd, hspi) == NO_ERR)
                   {
                       force_block_size_fat(&cmd, hspi);
                       return MMC;
                   }
                }
            }
        }
    }
    return card_version;
}

uint8_t init_sd_v2(R1response r1, R3response r3, Command_frame *cmd, SPI_HandleTypeDef *hspi )
{
    int init_sd_return = NO_ERR;
    uint16_t r3_cmd8_return = 0;
    if (check_if_valid(&(r3.r1_resp)))
    {
        // Send command ACMD41 (CMD55 + CMD41);
        // CMD41 sends a r1 response that must be 0x00, so we'll loop on ACMD41
        // Until we get what we want
        r3_cmd8_return = r3.ocr_arr[3] + (r3.ocr_arr[4] << 8);
        if (r3_cmd8_return == 0x1AA)
        {
            do {
                send_read_r1(hspi, &r1, cmd, App_cmd);
                send_read_r1(hspi, &r1, cmd, App_send_op_cond);
            } while (r1.full_response == 0x01);

            if (check_if_valid(&r1))
            {
                //Send command CMD58
                send_read_r3(hspi, &r3, cmd, Read_ocr);
                if (!check_if_valid(&(r3.r1_resp)))
                    init_sd_return = ERR_NO_RESP;
            }
            else
                init_sd_return = ERR_NO_RESP;
        }
        else
           init_sd_return = MISMATCH;
    }
    else
        init_sd_return = NOT_SD2;
   return init_sd_return;
}

__uint8_t init_sd_v1(R1response *r1, Command_frame *cmd, SPI_HandleTypeDef *hspi)
{
    do {
        send_read_r1(hspi, r1, cmd, App_cmd);
        send_read_r1(hspi, r1, cmd, App_send_op_cond);
    } while (r1->full_response == 0x01);
    if (!check_if_valid(r1))
    {
        return ERR_NO_RESP ;
    }
    return NO_ERR;
}

void force_block_size_fat(Command_frame *cmd, SPI_HandleTypeDef *hspi)
{
    R1response r1;
    set_command(cmd, Set_blocklen);
    cmd->command_args.args_ui32 = 0x200;
    send_command(cmd, hspi);
    read_r1_response(hspi, &r1);
}

enum init_sd_return init_mmc_v3(R1response *r1, Command_frame *cmd, SPI_HandleTypeDef *hspi)
{

    do {
        send_read_r1(hspi, r1, cmd, App_cmd);
        send_read_r1(hspi, r1, cmd, App_send_op_cond);
    } while (r1->full_response == 0x01);
    if (!check_if_valid(r1))
    {
        return ERR_NO_RESP ;
    }
    return NO_ERR;
}

// TODO : Fix : Doesnt work. Currently need to reset sd card after read attempt
void read_single_block(SPI_HandleTypeDef *hspi, __uint32_t address)
{
    R1response r1;
    Data_packet d;
    Command_frame cmd;
    set_command_with_args(&cmd, Read_single_block, address);
    send_command(&cmd, hspi);
    read_r1_response(hspi, &r1);
    if (check_if_valid(&r1))
    {
        // Read the data token
        //TODO Really need to implement some sort of TIMEOUT
        do
        {
            HAL_SPI_Receive(hspi, &d.data_token, 1*sizeof(__uint8_t), HAL_MAX_DELAY);
            // TODO Deal with error token
        } while (d.data_token == 0xFF);
        HAL_SPI_TransmitReceive(hspi, d.data_block, &HIGH_SPI, 512*sizeof(__uint8_t), HAL_MAX_DELAY);
        HAL_SPI_TransmitReceive(hspi, d.data_packet_crc, &HIGH_SPI, 2*sizeof(__uint8_t), HAL_MAX_DELAY);
    }
    int allo = 0;
    allo += 1;
}





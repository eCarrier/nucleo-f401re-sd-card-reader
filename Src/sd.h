//
// Created by etienne on 1/25/20.
//

#ifndef SD_CARD_READER_SD_H
#define SD_CARD_READER_SD_H

/*
 * A Command frame is 48 bits defined as the following :
 *  COMMAND ID :  [0][1] and the command number over 6 bits
 *  ARGS :        arguments over 32 bits
 *  CRC :         CRC over 7 bits with an end bit
 */

#include <stm32f4xx_hal.h>
#include "stdint.h"
#include "string.h"


typedef enum sd_version
{
    SD_VER1,
    SD_VER2,
    MMC,
    UNKNOWN_CARD

} Card_version ;

enum init_sd_return
{
    MISMATCH,
    ERR_NO_RESP,
    NO_ERR,
    NOT_SD2,
};

enum Error_token
{
   DATA_TRANSFER_ERROR = 0x0,
   DATA_TRANSFER_CC_ERROR = 0x0,
   DATA_TRANSFER_CARD_ECC_FAILED = 0x0,
   DATA_TRANSFER_OOR = 0x0,
   DATA_TRANSFER_LOCKED= 0x0
};

typedef enum cmd
{
    Go_idle_state =              0, // Software reset
    Send_op_cond =               1, // Start initialization process
    App_send_op_cond =          41, // ACMD : (SDC) Start initialization process
    Send_if_cond =               8, // Check voltage range
    Send_csd =                   9, // Read CSD register
    Send_cid =                  10, // Read CID register
    Stop_transmission =         12, // Stop to read data
    Set_blocklen =              16, // Change R/W block size
    Read_single_block =         17, // Read a block
    Read_multiple_block =       18, // Read multiple block
    Set_wr_block_erase_count =  23, // ACMD : Define number of blocks to pre-erase
    // with next multi-block write command.
            Write_block =               24, // Write a block
    Write_multiple_block =      25, // Write multiple block
    App_cmd =                   55, // For ACMD command (Command to send before each)
    Read_ocr =                  58  // Read OCR
} Command_number;



enum crc
{
    Go_idle_state_crc =         0x95,
    Send_if_cond_crc =          0x87,
    Default_crc =               0xFF
};

enum args
{
    No_args =                   0x00,
    Send_if_cond_args =         1 << 8 | 0xAA, // Rsv(0)[31:12], Supply Voltage(1)[11:8], Check Pattern(0xAA)[7:0]
    App_send_op_cond_args =     1 << 30 // Rsv(0)[31], HCS[30], Rsv(0)[29:0]
};

typedef struct
{
    uint8_t   command_id;
    union
    {
        uint8_t  args_ui8[4];
        uint32_t args_ui32;
    } command_args;
    uint8_t   command_crc; } Command_frame ;

/**
 * @Synopsis The union representing a R1 response
 */
typedef union
{
    struct
    {
        unsigned int in_idle_state :        1;
        unsigned int erase_Reset:           1;
        unsigned int illegal_command :      1;
        unsigned int command_crc_error :    1;
        unsigned int erase_sequence_error : 1;
        unsigned int address_error :        1;
        unsigned int parameter_error :      1;
        unsigned int zero_flag :            1;
    } fields ;
    uint8_t full_response;
} R1response ;

typedef struct
{
    __uint8_t data_token;
    __uint8_t data_block[512];
    __uint8_t data_packet_crc[2];
} Data_packet;

typedef union
{
    struct
    {
        unsigned int error:                     1;
        unsigned int cc_error:                  1;
        unsigned int ecc_failed:                1;
        unsigned int out_of_range:              1;
        unsigned int card_is_locked :           1;
        unsigned int zero_flag_1 :              1;
        unsigned int zero_flag_2 :              1;
        unsigned int zero_flag_3 :              1;
    };
    __uint8_t full_error;
} Data_packet_error_token;

/**
 * @Synopsis  The union representing a R3 response
 */
typedef struct {
    R1response r1_resp;
    uint8_t  ocr_arr[4];
} R3response ;

typedef struct sd_card_manager
{
    int is_initialized;
    Card_version card_version;
} Sd_card_manager;


//Communication commands
void set_command(Command_frame *cmd, Command_number command_number);
void set_command_with_args(Command_frame *cmd, Command_number command_number, uint32_t args);
void set_command_crc(Command_frame *cmd, Command_number command_number);
void send_command(Command_frame *cmd, SPI_HandleTypeDef *hspi);
void set_sd_card_to_spi(SPI_HandleTypeDef *hspi);
void read_r1_response(SPI_HandleTypeDef *hspi, R1response *r);
void read_r3_response(SPI_HandleTypeDef *hspi, R3response *r);
int check_if_valid (R1response *r);
Card_version init_sd_card (SPI_HandleTypeDef *hspi);
uint8_t init_sd_v2(R1response r1, R3response r3, Command_frame *cmd, SPI_HandleTypeDef *hspi );
__uint8_t init_sd_v1(R1response *r1, Command_frame *cmd, SPI_HandleTypeDef *hspi);
enum init_sd_return init_mmc_v3(R1response *r1, Command_frame *cmd, SPI_HandleTypeDef *hspi);
void force_block_size_fat(Command_frame *cmd, SPI_HandleTypeDef *hspi ) ;
void read_single_block(SPI_HandleTypeDef *hspi, __uint32_t address);

//Functions for fatfs
//Base functions are the following
//
//disk_status     - Get device status
//disk_initialize - Initialize device
//disk_read       - Read sector(s)
//disk_write      - Write sector(s)
//disk_ioctl      - Control device dependent functions
//get_fattime     - Get current time


#endif //SD_CARD_READER_SD_H

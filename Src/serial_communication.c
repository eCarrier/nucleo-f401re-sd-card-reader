//
// Created by etienne on 2/2/20.
//

#include "serial_communication.h"
#include <stdlib.h>

void serial_read(UART_HandleTypeDef *huart, uint8_t *buff, size_t buff_size)

{
    uint8_t i = 0;
    uint8_t rx_buff = 0;
    uint8_t tx_buff = 0;
    do
    {
        HAL_UART_Receive(huart, &rx_buff, 1* sizeof(char), HAL_MAX_DELAY);
        HAL_Delay(5);
        tx_buff = rx_buff ;
        HAL_UART_Transmit(huart, &tx_buff, 1*sizeof(char), HAL_MAX_DELAY);
        buff[i] = rx_buff;
    } while(rx_buff != '\r' && i++ < buff_size);
    // Add en EOL
    tx_buff = '\n' ;
    HAL_UART_Transmit(huart, &tx_buff, 1*sizeof(char), HAL_MAX_DELAY);
}

void serial_print(UART_HandleTypeDef *huart, const char * msg)
{
    __uint8_t tx_buff[50];
    strcpy((char *)tx_buff, msg);
    HAL_UART_Transmit(huart, tx_buff, strlen((char *)tx_buff), HAL_MAX_DELAY);
}

//
// Created by etienne on 2/2/20.
//

#ifndef SD_CARD_READER_SERIAL_COMMUNICATION_H
#define SD_CARD_READER_SERIAL_COMMUNICATION_H

#include <stm32f4xx_hal.h>
#include "string.h"

void serial_read(UART_HandleTypeDef *huart, __uint8_t *buff, size_t buff_size);
void serial_print(UART_HandleTypeDef *huart, const char * msg);


#endif //SD_CARD_READER_SERIAL_COMMUNICATION_H

# fatfs-stm32f4

stm32f4 implementation of **fatfs** for a Nucleo board F401RE

# Introduction

This project details the implementation of the low-level device control module
for fatfs.

I say *implementation of the low-level control modules* because fatfs is a
**middleware**. As the developper said :

```
Since FatFs module is the filesystem layer independent of platforms and storage media, it is completely separated from the physical devices, such as memory card, harddisk and any type of storage device. The low level device control module is not any part of FatFs module and it needs to be provided by implementer.
```

The projects is made using STM's HAL for a F401RE Nucleo and might work on
other platform with some tweeks.

I'm using STM32CubeMX and CLion

# Documentation

# Sources 

* http://elm-chan.org/fsw/ff/00index\_e.html

# Current tasks

* [X] Implement the rest of the init flow (reimplement init if needed)
    * [X] SDSC and SDHC (v2)
    * [X] SDSC v1
    * [X] MMC
* [X] Support for differents SD card (go further in the init flow)
* [X] Allow serial monitor so i'll be able to read and write different values/adresses
* [X] Implement Single block read
* [ ] Implement multiple block read
* [ ] Implement Single block write
* [ ] Implement multipl block write
* [ ] Do some comments + documentation
* [ ] add fatfs to project
* [ ] Read and implement status and initialization routines
* [ ] Explore the need for using a pullup resistor on DO
